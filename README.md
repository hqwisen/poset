# Data structures for Partially Ordered Sets

Initial implementation of a closed set data structure with the following partial orders:

- Include of subset partial order
- Lower or equal partial order for integer vectors

# Source

See `ClosedSet` class for the main data structure.

See `orders` package for the partial orders implementation.

See `bounds` package for bounds implementation.

# Test

There is (non-exhaustive) tests that test the basic operations of the closed set data structure, the partial orders
and the greatest lower bound.

# Perform the evaluation

```bash
tar xf distributions/poset.ar -C distributions/
python scripts/evaluation.py
```

# How to use the scripts

Start vcsn notebook:

```bash
docker run -d --name vcsn -p 8888:8888 -v ~/Projects/poset/scripts:/vcsn/poset-scripts lrde/vcsn:2.8
```

Convert HOA to SVG:

```bash
python3 aut-svg.py automaton.hoa
```

`randaut.py` Generate Random Automata (see TV05 reference of antichain-universality paper)

`genaut.py` Generate automata from `Ak` family defined in `antichain-universality` paper
