import os
import subprocess
import sys

DOT_BIN = 'dot'
AUTFILT_BIN = f"{os.environ['HOME']}/soft/spot/bin/autfilt"

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("No filename given")
        sys.exit(1)
    filename = sys.argv[1]
    output_filename = os.path.splitext(filename)[0] + '.svg'
    print("OUTPUT:", output_filename)
    autfilt_args = [AUTFILT_BIN, "--dot", "-8", filename]
    out, err = subprocess.Popen(autfilt_args, stdout=subprocess.PIPE).communicate()
    if err:
        print(err, file=sys.stderr)
    dot_args = [DOT_BIN, '-Tsvg', '-o', output_filename]
    out, err = subprocess.Popen(dot_args, stdin=subprocess.PIPE,
                                stdout=subprocess.PIPE).communicate(input=out)
    if err:
        print(err, file=sys.stderr)
