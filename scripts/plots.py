import json
import sys
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
# noinspection PyUnresolvedReferences
from mpl_toolkits import mplot3d

import utils

fig = plt.figure()
ax = plt.axes(projection='3d')

logger = utils.get_logger(__name__)

PLOTS_FOLDER = Path(utils.DATA_FOLDER) / Path('plots')

PLOTS_FOLDER.mkdir(parents=True, exist_ok=True)


def plot3d(result_file):
    with open(result_file) as f:
        data = json.load(f)
        print(data)
    x = np.array(list(data.keys()))
    y = np.array(list(data[next(iter(data.keys()))].keys()))
    Z = np.array([[data[x][y] for y in y] for x in x])
    x = np.array(list(map(float, x)))
    y = np.array(list(map(float, y)))
    X, Y = np.meshgrid(x, y)
    logger.debug(f"(r)  X = \n{X}")
    logger.debug(f"(f)  Y = \n{Y}")
    logger.debug(f"(ms) Z = \n{Z}")
    Z = np.transpose(Z)
    # FIXME why transpose ? (see Jupyter notebook)
    logger.debug(f"(ms) Z transpose = \n{Z}")
    ax = plt.axes(projection='3d')
    ax.plot_surface(X, Y, Z, rstride=1, cstride=1,
                    cmap='viridis', edgecolor='none')
    average_method = Path(result_file).name.split("__")[0]
    # ax.set_title(f'Result for {result_file}')
    ax.set_xlabel("Transition density (r)")
    ax.set_ylabel("Density of acc. states (f)")
    ax.set_zlabel(f"Exec. time {average_method} (ms)")
    ax.view_init(20, -120)
    # plt.show()
    plot_file = Path(result_file).with_suffix('.png')
    logger.info(f"Saving plot in {plot_file}")
    plt.savefig(plot_file, bbox_inches='tight')
    # tex_file = Path(result_file).with_suffix('.tex')
    # logger.info(f"Saving tikz (LaTeX) plot in {tex_file}")
    # tikzplotlib.save(tex_file)


def main():
    if len(sys.argv) < 2:
        print("Error: No result file given", file=sys.stderr)
        sys.exit(1)
    result_file = sys.argv[1]
    logger.info(f"Plotting file {result_file}")
    plot3d(result_file)


if __name__ == '__main__':
    main()
