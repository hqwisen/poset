import glob
import json
import subprocess
import sys
import time
from pathlib import Path

import utils
from randaut import randtv05
from utils import DATA_FOLDER

logger = utils.get_logger(__name__)


# Owl configuration


def execowl(autstring, duration_from_owl=True):
    """
    :param duration_from_owl: Get duration time from Owl output
    :param autstring: HOA (string) automaton representation
    :return: Tuple: (is_universal, elapsed_time) where elapsed time is in ms
    """
    cmd = [utils.OWL_CMD,
           '---', 'hoa', '---', 'complete', '---',
           'universality-check',
           '---', 'string']
    logger.debug(f"Running cmd: {cmd}")
    pipe = subprocess.Popen(cmd, stdout=subprocess.PIPE, stdin=subprocess.PIPE,
                            universal_newlines=True)
    started = time.time()
    stdout, stderr = pipe.communicate(input=autstring)
    delta = time.time() - started
    is_universal, duration = stdout.strip().lower().split()
    duration = int(duration)
    logger.debug(f"univ. check: {is_universal} with duration {duration}ms")
    logger.debug(f"Took {delta} to check")
    if is_universal not in ['true', 'false']:
        # logger.error("Unexpected error, returned nor true or false")
        raise ValueError(f"Invalid boolean string: {is_universal}")
    return is_universal == 'true', duration if duration_from_owl else delta * 1000  # in ms


def execvcsn(autstring):
    """
    :param autstring: VCSN (string) automaton representation
    :return: Tuple: (is_universal, elapsed_time) where elapsed time is in ms.
    """
    try:
        import vcsn
    except ImportError:
        print("vcsn not installed, abort.", file=sys.stderr)
        sys.exit(1)
    aut = vcsn.automaton(autstring, strip=False)

    def universality_check(a):
        # remove useless state -> det. -> min. -> add useless transitions -> complement
        # complement = a.trim().determinize().minimize().complete().complement()
        complement = a.determinize().complete().complement()
        # is_useless -> automaton accepts no words
        # so if the complement is useless -> original is universal
        return complement, complement.is_useless()

    started = time.time()
    _, is_universal = universality_check(aut)
    delta = time.time() - started
    return is_universal, delta * 1000  # in ms


def check_dataset(dataset, validated_file, formatt='hoa', execuniv=execowl):
    files = glob.glob(f"{DATA_FOLDER}/{dataset}/*.{formatt}")
    logger.info(f"Checking dataset {dataset} with {execuniv.__name__} "
                f"function and validated file {validated_file}")
    with open(validated_file) as f:
        validated = json.load(f)
    for filename in files:
        logger.debug(f"Checking file {filename}")
        path = Path(filename)
        name = path.stem
        with open(filename) as f:
            content = f.read()
            is_universal, elapsed = execuniv(content)
            try:
                logger.debug(f"Checked {name} with {execuniv.__name__} -> "
                             f"is_universal {is_universal} =? {validated[name][0]}")
                if is_universal != validated[name][0]:
                    logger.error(f"{name} does not map validation!")
            except KeyError:
                logger.warning(f"{name} is not in validated file, ignoring")


def check_tv05():
    hoa = randtv05(nstates=175, density_of_accstates=1, transition_density=1)
    execowl(hoa)


def test_file():
    automata = [{
        'file': f'{utils.DATA_FOLDER}/tv05/tv05.hoa'
    }]
    for automaton in automata:
        cmd = [utils.OWL_CMD, '-I', automaton['file'],
               '---', 'hoa', '---',
               'universality-check',
               '---', 'string']
        result = subprocess.run(cmd, stdout=subprocess.PIPE)
        is_universal = result.stdout.decode('utf-8').strip()
        logger.debug(f"{automaton['file']} univ. check: {is_universal}")


if __name__ == "__main__":
    check_dataset('tv05-dataset0',
                  'data/evaluation/vcsn__univcheck__tv05-dataset0__results.json')
