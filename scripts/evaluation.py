import argparse
import datetime
import itertools
import json
import os

import numpy as np

import utils
from randaut import randtv05
from universality import execowl, execvcsn

logger = utils.get_logger(__name__)

RESULTS_DIR = os.path.join(utils.DATA_FOLDER, 'evaluation',
                           datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S'))

os.makedirs(RESULTS_DIR, exist_ok=True)


def evaluate(auts=100, states=30, average_method='median', algo='owl'):
    """
    average_method must be in ['median', 'mean']
    Result format:
    result(x, y) = z

    x = Transition density (r)
    y = Density of final states (f)
    z = Median time (ms)
    """
    if algo == 'owl':
        execfunc, autformat = execowl, 'hoa'
    elif algo == 'vcsn':
        execfunc, autformat = execvcsn, 'vcsn'
    else:
        raise ValueError(f"Unknown algo {algo}")
    nstates, nauts = states, auts
    densities_of_accstates = (0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1)
    transition_densities = (0, 0.25, 0.5, 0.75, 1, 1.25, 1.5, 1.75, 2,
                            2.25, 2.5, 2.75, 3, 3.25, 3.5, 3.75, 4)
    logger.info(f"Running with: \n"
                f"# of random gen automata: {nauts}\n"
                f"# states (per gen. aut.): {nstates}\n"
                f"# Average method: {average_method}\n"
                f"# Algo: {algo}\n"
                f"Densities of acc. states: {densities_of_accstates}\n"
                f"Transition densities: {transition_densities}")
    results = {r: {} for r in transition_densities}
    for f, r in itertools.product(densities_of_accstates, transition_densities):
        logger.info(f"Generating and checking {nauts} with settings: f={f}, r={r}")
        elapsed_times = []
        for i in range(nauts):
            aut = randtv05(nstates, density_of_accstates=f, transition_density=r)
            is_universal, elapsed_time = execfunc(aut[autformat])
            elapsed_times.append(elapsed_time)
        results[r][f] = getattr(np, average_method)(elapsed_times)
    result_file = os.path.join(RESULTS_DIR,
                               f'{average_method}__nauts_{nauts}__nstates_{nstates}.json')
    logger.debug(f"Results:\n{results}")
    logger.info(f"Writing results in {result_file}")
    with open(result_file, 'w') as f:
        json.dump(results, f, indent=2)


def parse_args():
    defaults = {
        'states': 175,
        'auts': 100,
        'average-method': 'mean',
        'algo': 'owl'
    }

    parser = argparse.ArgumentParser(description='Evaluate Universality Check using Owl')
    parser.add_argument('--states', '-s', type=int, default=defaults['states'],
                        help=f"Number of states per random automaton, "
                        f"default={defaults['states']}")
    parser.add_argument('--auts', '-a', type=int, default=defaults['auts'],
                        help=f"Number of random automata to generate per test, "
                        f"default={defaults['auts']}")
    parser.add_argument('--average-method', '-am', type=str, default=defaults['average-method'],
                        choices=['median', 'mean'],
                        help=f"Average method to use, "
                        f"default={defaults['average-method']}")
    parser.add_argument('--algo', '-al', type=str, default=defaults['algo'],
                        choices=['owl', 'vcsn'],
                        help=f"Algorithm to use. Owl uses the antichain algorithm "
                        f"and VCSN the subset construction, "
                        f"default={defaults['algo']}")
    return vars(parser.parse_args())


if __name__ == '__main__':
    evaluate(**parse_args())
