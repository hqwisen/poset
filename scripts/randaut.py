import os

import numpy as np
from jinja2 import FileSystemLoader, Environment

import utils

logger = utils.get_logger(__name__)

# jinja template
loader = FileSystemLoader(searchpath=utils.TEMPLATE_FOLDER)
env = Environment(loader=loader)


def randtv05(nstates, density_of_accstates, transition_density, alphabet=('0', '1'), autid=0):
    # FIXME ceil or floor ?
    ntranstions = int(np.floor(transition_density * nstates))
    naccstates = int(np.floor(density_of_accstates * nstates))
    # Last states (id = nstates) = sink state
    states = np.arange(nstates)
    sink_state = nstates
    logger.debug("Generating random automaton with the following settings:")
    logger.debug(f"Number of states: {nstates}")
    logger.debug(f"Density of accepting states: {density_of_accstates}")
    logger.debug(f"Transition density: {transition_density}")
    logger.debug(f"Number of transitions: {ntranstions}")
    logger.debug(f"Number of accepting states: {naccstates}")
    accstates = np.random.choice(states, size=naccstates, replace=False)
    initialstate = states[0]
    logger.debug("Accepting states: %s", accstates)
    pairs = np.array([(a, b) for a in states for b in states])

    # logger.debug(f"All pairs: {pairs}, shape[0] {pairs.shape[0]}")

    def choose_pairs():
        size = ntranstions
        if ntranstions > pairs.shape[0]:
            logger.warning(f"No enough pairs for transition density: "
                           f"r={transition_density}, nstates={nstates} "
                           f"which gives # of transitions = {ntranstions} "
                           f"for only {pairs.shape[0]} pairs")
            size = pairs.shape[0]
        indices = np.random.choice(pairs.shape[0], size=size, replace=False)
        return indices

    letter_transitions = {letter: [pairs[i] for i in
                                   choose_pairs()]
                          for letter in alphabet}

    # Last state_transitions will remain empty, as it is the sink state
    state_transitions = [[] for _ in range(nstates + 1)]
    for letter in letter_transitions:
        for source, dest in letter_transitions[letter]:
            state_transitions[source].append([letter, dest])
    logger.debug(f"State transitions: {state_transitions}")
    # Add transition to sink states, if required
    for source in range(nstates):
        outgoing_letters = {l for l, _ in state_transitions[source]}
        for letter in alphabet:
            if letter not in outgoing_letters:
                state_transitions[source].append([letter, sink_state])
    # Render template
    data_folder = os.path.join(utils.DATA_FOLDER, 'tv05')
    # tv05_file = os.path.join(data_folder, f"tv05.hoa")
    # with open(tv05_file, 'w') as f:
    generated = {}
    for autformat in ['hoa', 'vcsn', 'digraph']:
        template = env.get_template(f'tv05.{autformat}.j2')
        content = template.render(nstates=nstates + 1, initialstate=initialstate,
                                  accstates=accstates, transitions=state_transitions,
                                  autid=autid)
        generated[autformat] = content
    # logger.info(f"Rendering {tv05_file}")
    # logger.debug(f"Rendered: \n{hoa_content})")
    # f.write(hoa_content)
    return generated


def genaut(nauts=1, nstates=10):
    data_folder = os.path.join(utils.DATA_FOLDER, 'tv05')
    os.makedirs(data_folder, exist_ok=True)
    logger.info(f"Generating {nauts} automata in {data_folder}")
    for i in range(nauts):
        logger.debug(f"Generating aut. #{i}")
        generated = randtv05(nstates=nstates, density_of_accstates=1, transition_density=1, autid=i)
        for autformat in generated:
            filename = os.path.join(data_folder, f"aut{i}.{autformat}")
            logger.debug(f"Writing {filename}")
            with open(filename, 'w') as f:
                f.write(generated[autformat])


if __name__ == "__main__":
    genaut(100)
