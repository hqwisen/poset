import logging
import os

DATA_FOLDER = os.path.join(os.getcwd(), 'data')

DEFAULT_LEVEL = logging.INFO

OWL_CMD = "distributions/bin/poset"
# os.path.join(os.getcwd(), 'owl', 'bin', 'owl')

TEMPLATE_FOLDER = os.path.join(os.getcwd(), 'scripts', 'templates')


def get_logger(name):
    FORMAT = '%(asctime)s - %(levelname)s - %(module)s - %(message)s'
    logger = logging.getLogger(name)
    formatter = logging.Formatter(fmt=FORMAT)
    handlers = [logging.StreamHandler()]
    # handlers.append(logging.FileHandler(filepath))
    for handler in handlers:
        handler.setFormatter(formatter)
        logger.addHandler(handler)
    logger.setLevel(DEFAULT_LEVEL)
    return logger
