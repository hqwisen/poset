import logging
import os

from jinja2 import Environment, FileSystemLoader

# Configure logging

FORMAT = '%(asctime)s - %(levelname)s - %(module)s - %(message)s'
LEVEL = logging.INFO

logger = logging.getLogger(__name__)
formatter = logging.Formatter(fmt=FORMAT)
handlers = [logging.StreamHandler()]
# handlers.append(logging.FileHandler(filepath))
for handler in handlers:
    handler.setFormatter(formatter)
    logger.addHandler(handler)
logger.setLevel(LEVEL)

TEMPLATE_FOLDER = f"{os.environ['HOME']}/Projects/poset/scripts/templates"


def main():
    min_k, max_k = 2, 50
    loader = FileSystemLoader(searchpath="scripts/templates")
    env = Environment(loader=loader)
    template = env.get_template('automaton-Ak.hoa.j2')
    data_folder = os.path.join(DATA_FOLDER, 'automata')
    for k in range(min_k, max_k + 1):
        ak_file = os.path.join(data_folder, f"A{k}.hoa")
        with open(ak_file, 'w') as f:
            hoa_content = template.render(k=k)
            logger.info(f"Rendering for k={k}, file: {ak_file}")
            logger.debug(f"Rendered: \n ${hoa_content})")
            f.write(template.render(k=k))


if __name__ == "__main__":
    main()
