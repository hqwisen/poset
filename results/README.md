Command example:

```bash
nohup time python scripts/evaluation.py -am mean -al owl > nohups/expX-owl-mean.out&
```

Machines: yoda, worker1

# Experiment 1

Results are checking VCSN and Owl using the following configuration:

```
densities_of_accstates = (0, 0.2, 0.4, 0.6, 0.8, 1)
transition_densities = (0, 0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4)
```

Time in Owl is taken for the execution of the `backward()` method.


Time in VCSN is taken for the execution of the `universality_check()` function.

# Experiment 2

```
densities_of_accstates = (0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1)
transition_densities = (0, 0.25, 0.5, 0.75, 1, 1.25, 1.5, 1.75, 2,
                        2.25, 2.5, 2.75, 3, 3.25, 3.5, 3.75, 4)
```

Time in Owl is taken for the execution of the backward loop (without `getAcceptingStates()` computation).

# Experiment 3

Rerun of exp2 with logging removed in UniversalityCheck.java

# Experiment 4

Rereun of exp3 with `nanoTime()` replaced to `currentTimeMillis()`

# Experiment 5

`worker1`

Run using `optimization` branch with `HashMap` for transition functions

# Experiment 6

`worker1`

Run using `master` branch without `HashMap` for transition functions 
(just to see that `yoda` had no influence on the results in the first experiments)
