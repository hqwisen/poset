package poset;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.junit.Assert.assertThat;

import java.util.Set;
import org.junit.Before;
import org.junit.Test;
import poset.orders.Include;


public class AntichainListTest {
  AntichainList<Set<Integer>> al;

  @Before
  public void beforeEachTest() {
    al = new AntichainList<>(Include::order);
  }

  @Test
  public void add() {
    al.add(Set.of(10));
    assertThat(al, hasItem(Set.of(10)));
    al.add(Set.of(8, 5));
    assertThat(al, hasItem(Set.of(8, 5)));
    al.add(Set.of(1, 7));
    assertThat(al, hasItem(Set.of(1, 7)));
  }

  @Test
  public void addAll() {
    al.add(Set.of(10));
    assertThat(al, hasItem(Set.of(10)));
    al.addAll(Set.of(Set.of(1, 7), Set.of(8, 5)));
    assertThat(al, hasItem(Set.of(10)));
    assertThat(al, hasItem(Set.of(1, 7)));
    assertThat(al, hasItem(Set.of(8, 5)));
  }
}
