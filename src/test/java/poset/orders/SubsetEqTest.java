package poset.orders;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Set;
import org.junit.Before;
import org.junit.Test;
import poset.orders.SubsetEq;

public class SubsetEqTest {

  public SubsetEq<Integer> subsetEq;


  @Before
  public void before() {
    subsetEq = new SubsetEq<>();
  }

  @Test
  public void testSubsetEqOrder() {
    Set<Set<Integer>> setOfSet0 = Set.of(
        Set.of(0), Set.of(0, 2), Set.of(0, 1), Set.of(1, 2)
    );
    Set<Set<Integer>> setOfSet1 = Set.of(
        Set.of(0), Set.of(0, 1, 2)
    );

    assertTrue(subsetEq.test(setOfSet0, setOfSet1));
    assertFalse(subsetEq.test(setOfSet1, setOfSet0));

    // All set in a set of sets are at least included in itself
    assertTrue(subsetEq.test(setOfSet0, setOfSet0));
    assertTrue(subsetEq.test(setOfSet1, setOfSet1));

  }
}
