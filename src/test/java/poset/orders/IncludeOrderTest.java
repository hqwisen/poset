package poset.orders;

import org.junit.Before;
import org.junit.Test;
import poset.exceptions.Incomparable;

import java.util.Set;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


public class IncludeOrderTest {

    private Set<Integer> sempty, s1, s12, s13, s123, s456, s4;
    private Set<String> ssempty, ssUniv, ssULB, ssUCL;


    @Before
    public void beforeEachTest() {
        sempty = Set.of();
        s1 = Set.of(1);
        s12 = Set.of(1, 2);
        s13 = Set.of(1, 3);
        s123 = Set.of(1, 2, 3);
        s456 = Set.of(4, 5, 6);
        s4 = Set.of(4);
        ssempty = Set.of();
        ssUniv = Set.of("Université");
        ssULB = Set.of("Université", "Libre", "de", "Bruxelles");
        ssUCL = Set.of("Université", "Catholique", "de", "Louvain");
    }

    @Test
    public void EverythingMustIncludeEmptySubset() {
        Include<Integer> include = new Include<>();
        Include<String> includeStr = new Include<>();

        assertTrue(include.test(sempty, s1));
        assertTrue(include.test(sempty, s12));
        assertTrue(include.test(sempty, s123));
        assertTrue(include.test(sempty, s456));
        assertTrue(include.test(sempty, s4));

        assertTrue(includeStr.test(ssempty, ssUniv));
        assertTrue(includeStr.test(ssempty, ssULB));
        assertTrue(includeStr.test(ssempty, ssUCL));
    }

    @Test
    public void EmptySubsetIncludesNothing() {
        Include<Integer> include = new Include<>();
        Include<String> includeStr = new Include<>();

        assertFalse(include.test(s1, sempty));
        assertFalse(include.test(s12, sempty));
        assertFalse(include.test(s123, sempty));
        assertFalse(include.test(s456, sempty));
        assertFalse(include.test(s4, sempty));

        assertFalse(includeStr.test(ssUniv, ssempty));
        assertFalse(includeStr.test(ssULB, ssempty));
        assertFalse(includeStr.test(ssUCL, ssempty));
    }

    @Test
    public void InclusionWithSubsetOfIntegers_MustInclude() {
        Include<Integer> include = new Include<>();
        assertTrue(include.test(s1, s12));
        assertTrue(include.test(s1, s13));
        assertTrue(include.test(s1, s123));
        assertTrue(include.test(s12, s123));
        assertTrue(include.test(s13, s123));
        assertTrue(include.test(s4, s456));
    }


    @Test
    public void InclusionWithSubsetOfIntegers_MustNotInclude() {
        Include<Integer> include = new Include<>();
        assertFalse(include.test(s12, s1));
        assertFalse(include.test(s13, s1));
        assertFalse(include.test(s123, s1));
        assertFalse(include.test(s123, s12));
        assertFalse(include.test(s123, s13));
        assertFalse(include.test(s456, s4));

        // compare => false if incomparable
        assertFalse(include.test(s12, s13));
        assertFalse(include.test(s13, s12));
    }

    @Test
    public void InclusionWithSubsetOfStrings_MustInclude() {
        Include<String> include = new Include<>();
        assertTrue(include.test(ssUniv, ssULB));
        assertTrue(include.test(ssUniv, ssUCL));
    }

    @Test
    public void InclusionWithSubsetOfStrings_MustNotInclude() {
        Include<String> include = new Include<>();
        assertFalse(include.test(ssULB, ssUniv));
        assertFalse(include.test(ssUCL, ssUniv));

        // compare => false if incomparable
        assertFalse(include.test(ssULB, ssUCL));
        assertFalse(include.test(ssUCL, ssULB));
    }

    @Test(expected = Incomparable.class)
    public void InclusionWithSubsetOfStrings_Incomparable() throws Incomparable {
        Include<String> include = new Include<>();
        include.compare(ssULB, ssUCL);
    }

    @Test(expected = Incomparable.class)
    public void InclusionWithSubsetOfIntegers_Incomparable() throws Incomparable {
        Include<Integer> include = new Include<>();
        include.compare(s12, s13);
    }


}
