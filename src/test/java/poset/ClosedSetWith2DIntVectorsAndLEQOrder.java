//package poset;
//
//import poset.order.LEQ;
//import poset.types.Vector;
//import org.junit.Before;
//import org.junit.Test;
//
//import static org.junit.Assert.*;
//
//
//public class ClosedSetWith2DIntVectorsAndLEQOrder {
//    private ClosedSet<Vector<Integer>> closedSet;
//
//    @Before
//    public void beforeEachTest() {
//        closedSet = new ClosedSet<>(new LEQ());
//    }
//
//    @Test
//    public void EmptyClosedSet(){
//        ClosedSet<Vector<Integer>> cl = new ClosedSet<>(new LEQ());
//        assertTrue(cl.isEmpty());
//        cl.add(new Vector<>(1, 0));
//        assertFalse(cl.isEmpty());
//    }
//
//    @Test
//    public void Add_SymbolcSizeChanges() {
//        closedSet.add(new Vector<>(3, 4));
//        assertEquals(1, closedSet.symbolicSize());
//        closedSet.add(new Vector<>(1, 2));
//        assertEquals(1, closedSet.symbolicSize());
//        closedSet.add(new Vector<>(4, 3));
//        assertEquals(2, closedSet.symbolicSize());
//    }
//
//    @Test
//    public void AddDuplicate_SymbolicSizeChanges() {
//        closedSet.add(new Vector<>(3, 4));
//        assertEquals(1, closedSet.symbolicSize());
//        closedSet.add(new Vector<>(3, 4));
//        assertEquals(1, closedSet.symbolicSize());
//    }
//
//    @Test
//    public void Add_ContainAddedElements() {
//        Vector<Integer> v34 = new Vector<>(3, 4);
//        Vector<Integer> v12 = new Vector<>(1, 2);
//        closedSet.add(v34);
//        assertTrue(closedSet.contains(v34));
//        assertTrue(closedSet.contains(new Vector<>(3, 4)));
//        closedSet.add(v12);
//        assertTrue(closedSet.contains(v12));
//        assertTrue(closedSet.contains(new Vector<>(1, 2)));
//    }
//
//    @Test
//    public void UnionOfTwoClosedSets() {
//        ClosedSet<Vector<Integer>> cs0, cs1, csu;
//        Vector<Integer> v12, v34, v1013;
//        v12 = new Vector<>(1, 2);
//        v34 = new Vector<>(3, 4);
//        v1013 = new Vector<>(10, 13);
//        cs0 = new ClosedSet<>(new LEQ());
//        cs1 = new ClosedSet<>(new LEQ());
//        cs0.add(v12);
//        cs1.add(v34);
//        cs0.add(v1013);
//        csu = cs0.union(cs1);
//        assertTrue(csu.contains(v12));
//        assertTrue(csu.contains(v34));
//        assertTrue(csu.contains(v1013));
//    }
//}
