package poset;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.util.List;
import org.junit.Test;
import poset.bounds.Bound;
import poset.bounds.GreatestLowerBound;
import poset.orders.LEQ;
import poset.orders.Order;

public class GreatestLowerBoundWith2DIntVectorsTest {

  @Test
  public void TestGLBWitheLEQOrder() {
    List<Integer> v67, v72, expectedGLB;
    Bound<List<Integer>> bound;
    Order<List<Integer>> order;
    bound = new GreatestLowerBound();
    order = new LEQ();
    v67 = List.of(6, 7);
    v72 = List.of(7, 2);
    expectedGLB = List.of(6, 2);
    assertFalse(order.areComparable(v67, v72));
    assertEquals(expectedGLB, bound.compute(v67, v72, order));
  }

}
