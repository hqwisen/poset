//package poset;
//
//import org.junit.Before;
//import org.junit.Test;
//import poset.order.Include;
//
//import java.util.Set;
//
//import static org.junit.Assert.assertEquals;
//import static org.junit.Assert.assertTrue;
//
//public class ClosedSetWithIntSubsetAndIncludeOrder {
//
//    private ClosedSet<Set<Integer>> closedSet;
//
//    @Before
//    public void beforeEachTest() {
//        closedSet = new ClosedSet<>(new Include<>());
//    }
//
//    @Test
//    public void Add_SymbolcSizeChanges() {
//        closedSet.add(Set.of(3, 4, 5));
//        assertEquals(1, closedSet.symbolicSize());
//        closedSet.add(Set.of(4, 3));
//        assertEquals(1, closedSet.symbolicSize());
//        closedSet.add(Set.of(1, 2));
//        assertEquals(2, closedSet.symbolicSize());
//        closedSet.add(Set.of(1));
//        assertEquals(2, closedSet.symbolicSize());
//    }
//
//    @Test
//    public void AddDuplicate_SymbolicSizeChanges() {
//        closedSet.add(Set.of(1, 2));
//        assertEquals(1, closedSet.symbolicSize());
//        closedSet.add(Set.of(1, 2));
//        assertEquals(1, closedSet.symbolicSize());
//    }
//
//    @Test
//    public void Add_ContainAddedElements() {
//        Set<Integer> s12 = Set.of(1, 2);
//        Set<Integer> s34 = Set.of(3, 4);
//        closedSet.add(s12);
//        assertTrue(closedSet.contains(s12));
//        assertTrue(closedSet.contains(Set.of(1, 2)));
//        closedSet.add(s34);
//        assertTrue(closedSet.contains(s34));
//        assertTrue(closedSet.contains(Set.of(3, 4)));
//    }
//
//}
