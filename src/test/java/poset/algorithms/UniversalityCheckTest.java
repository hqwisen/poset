//package poset.algorithms;
//
//import static org.junit.Assert.assertFalse;
//import static poset.algorithms.UniversalityCheck.*;
//
//import java.util.Set;
//import org.junit.Before;
//import org.junit.Test;
//import org.leibnizcenter.nfa.Event;
//import org.leibnizcenter.nfa.NFA;
//import org.leibnizcenter.nfa.State;
//import poset.ClosedSet;
//import poset.automaton.Automaton;
//import poset.automaton.NameEvent;
//import poset.automaton.NameState;
//import poset.order.Include;
//import poset.order.Order;
//
//public class UniversalityCheckTest {
//
//  private Automaton universalAutomaton, nonUniversalAutomaton,
//      universalSingleton, nonUniversalSingleton;
//  private State L0, L1, L2, L3;
//  private Event<State> zero, one;
//  private Set<Event<State>> alphabet;
//
//  @Before
//  public void before() {
//    L0 = new NameState("l0");
//    L1 = new NameState("l1");
//    L2 = new NameState("l2");
//    L3 = new NameState("l3");
//    zero = new NameEvent("0");
//    one = new NameEvent("1");
//    var defaultNonUniversalNFA = new NFA.Builder<State, Event<State>>()
//        .addTransition(L0, one, L0)
//        .addTransition(L0, one, L1)
//        .addTransition(L1, zero, L2)
//        .addTransition(L1, one, L2)
//        .addTransition(L2, zero, L3)
//        .addTransition(L2, one, L3)
//        .addTransition(L3, zero, L3)
//        .addTransition(L3, one, L3);
//
//    var universalNFA = defaultNonUniversalNFA
//        .addTransition(L0, one, L0)
//        .build();
//    var nonUniversalNFA = new NFA.Builder<State, Event<State>>()
//        .addTransition(L0, zero, L0)
//        .addTransition(L0, one, L1)
//        .build();
//    var universalSingletonNFA = new NFA.Builder<State, Event<State>>()
//        .addTransition(L0, zero, L0)
//        .addTransition(L0, one, L0)
//        .build();
//    var nonUniversalSingletonNFA = new NFA.Builder<State, Event<State>>()
//        .addTransition(L0, zero, L0)
//        .build();
//
//    alphabet = Set.of(zero, one);
//    universalAutomaton = new Automaton(universalNFA, Set.of(L0), Set.of(L0, L1, L2), alphabet);
//    nonUniversalAutomaton = new Automaton(nonUniversalNFA, Set.of(L0), Set.of(L0), alphabet);
//    universalSingleton = new Automaton(universalSingletonNFA, Set.of(L0), Set.of(L0), alphabet);
//    nonUniversalSingleton = new Automaton(nonUniversalSingletonNFA, Set.of(L0), Set.of(L0),
//        alphabet);
//  }
//
//  // NOTE: The automaton used in the following tests in A_3 from the paper DWDHR06
//
//  @Test
//  public void test_cpre_Operation() {
//    var automaton = universalAutomaton;
//    // Test with for cpre(1, {L3})
//    assertEquals(Set.of(L2, L3), cpre(automaton, zero, Set.of(L3)));
//    // Test with for cpre(0, {L3})
//    assertEquals(Set.of(L2, L3), cpre(automaton, one, Set.of(L3)));
//    // Test with for cpre(0, {L2})
//    assertEquals(Set.of(L1), cpre(automaton, zero, Set.of(L2)));
//    // Test with for cpre(1, {L2})
//    assertEquals(Set.of(L1), cpre(automaton, one, Set.of(L2)));
//    // Test with for cpre(1, {L1})
//    assertEquals(Set.of(L0), cpre(automaton, one, Set.of(L1)));
//    // Test with for cpre(0, {L1})
//    assertEquals(Set.of(), cpre(automaton, zero, Set.of(L1)));
//
//    // Test with for cpre(0, {L0})
//    assertEquals(Set.of(L0), cpre(automaton, zero, Set.of(L0)));
//    // Test with for cpre(1, {L0})
//    assertEquals(Set.of(L0), cpre(automaton, zero, Set.of(L0)));
//  }
//
//  /**
//   * Test with automaton FromSildes.hoa with targets = {{6}}.
//   * 8 states.
//   */
//  @Test
//  public void test_cpre_Operation() {
//    // 8 states, from 1 to 8
//    assertEquals(Set.of(4), cpre(automaton, one, Set.of(6)));
//    // 7 has two transition -> 0, to 6 and 8, so not in this cpre
//    assertEquals(Set.of(), cpre(automaton, zero, Set.of(6)));
//  }
//  /**
//   * Test with automaton FromSildes.hoa with targets = {{6, 7}}.
//   * 8 states.
//   */
//  @Test
//  public void test_cpre_Operation() {
//    // 8 states, from 1 to 8
//    assertEquals(Set.of(4, 5), cpre(automaton, one, Set.of(6, 7)));
//    // 7 has two transition -> 0, to 6 and 8, so not in this cpre
//    assertEquals(Set.of(), cpre(automaton, zero, Set.of(6, 7)));
//  }
//
//  @Test
//  public void test_CPre_Operation() {
//    var automaton = universalAutomaton;
//    var destinations = Set.of(
//        Set.of(L0, L1, L2)
//    );
//    // TODO implement
////        System.out.println(CPre(automaton, destinations));
//
//    //
//    Order<Set<State>> order = new Include<>();
//    ClosedSet<Set<State>> closedSet = new ClosedSet<>(order);
//    closedSet.add(Set.of(L0));
//    closedSet.add(Set.of(L1));
//    closedSet.add(Set.of(L0, L1));
//    System.out.println(closedSet);
//    System.out.println(order.compare(Set.of(L0), Set.of(L0, L1)));
//    automaton = nonUniversalAutomaton;
//    destinations = Set.of(
//        Set.of(L1)
//    );
////        System.out.println(CPre(automaton, destinations));
//
////        assertEquals(Set.of(L1, L0), Set.of(L0, L1));
////        assertEquals(CPre(automaton, destinations), Set.of(Set.of(L0, L1)));
//  }
//
//  @Test
//  public void testAntichainBackwardAlgorithm() {
//    // NOTE Those not are deprecated!!!
//    // The following test is failing with the backward algorithms
//    // After some manual debugging here are the potential reasons:
//    // - Is the negation of subsetEq the implementation that should be used as refered in Algo. 1
//    // of the paper ?
//    //   What happened if complementary of final states is empty i.e. Frontier = {} or Frontier =
//    //   {{}} ?
//    // - Maybe having access to universality implementation could be intersting as I am not sure
//    // focusing on the
//    //   algo implementation is the priority but implementation of the framework itself.
//
//    // ACTION: Try to understand universality problem with the 4 (rather simple) automata defined
//    // in this test.
//    // Result: Debug and manually debug: seems to work for universalAutomaton. But not sure if
//    // algo. implemented
//    //         correctly (more specifically the initial state)
//
////        assertTrue(backwardAlgorithm(universalAutomaton));
////        assertTrue(backwardAlgorithm(universalSingleton));
////        assertFalse(backwardAlgorithm(nonUniversalAutomaton));
////        assertFalse(backwardAlgorithm(nonUniversalSingleton));
//
//    var nfa = new NFA.Builder<State, Event<State>>()
//        .addTransition(L0, zero, L0)
//        .addTransition(L0, one, L0)
//        .addTransition(L0, one, L1)
//        .addTransition(L1, zero, L1)
//        .addTransition(L1, one, L1)
//        .build();
//    var automaton = new Automaton(nfa, Set.of(L0), Set.of(L0), alphabet);
//    assertFalse(backwardAlgorithm(automaton));
//  }
//
//  @Test
//  public void testAntichainBackwardAlgorithm_Universal() {
////        backwardAlgorithm(universalAutomaton);
//    var A = universalAutomaton;
//    var predecessors = cpre(A, one, Set.of(L1, L2, L3));
//    System.out.println(predecessors);
//    System.out.println(post(A, one, predecessors));
//  }
//}
