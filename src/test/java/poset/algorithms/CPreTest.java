package poset.algorithms;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import it.unimi.dsi.fastutil.ints.Int2ObjectLinkedOpenHashMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import java.util.BitSet;
import java.util.HashSet;
import java.util.Set;
import jhoafparser.parser.generated.ParseException;
import org.junit.Before;
import org.junit.Test;
import owl.automaton.Automaton;
import owl.automaton.AutomatonReader;
import owl.automaton.AutomatonReader.HoaState;
import owl.automaton.acceptance.OmegaAcceptance;
import owl.run.DefaultEnvironment;

public class CPreTest {

  private static final BitSet zero, one;

  private static final String FROMSLIDES = "HOA: v1\n"
      + "name: \"GFa\"\n"
      + "States: 8\n"
      + "Start: 0\n"
      + "acc-name: Buchi\n"
      + "Acceptance: 1 Inf(0)\n"
      + "AP: 2 \"0\" \"1\"\n"
      + "--BODY--\n"
      + "State: 0 \"1\" {0}\n"
      + "[0] 0\n"
      + "[1] 1\n"
      + "[0] 2\n"
      + "State: 1 \"2\" {0}\n"
      + "[1] 3\n"
      + "[0 | 1] 4\n"
      + "State: 2 \"3\" {0}\n"
      + "[0] 0\n"
      + "[0 | 1] 4\n"
      + "State: 3 \"4\" {0}\n"
      + "[1] 5\n"
      + "[0 | 1] 6\n"
      + "State: 4 \"5\" {0}\n"
      + "[0 | 1] 6\n"
      + "[0] 7\n"
      + "State: 5 \"6\"\n"
      + "[0 | 1] 7\n"
      + "State: 6 \"7\"\n"
      + "[0] 5\n"
      + "[0 | 1] 7\n"
      + "State: 7 \"8\" {0}\n"
      + "[0 | 1] 7\n"
      + "--END--";

  private static final String AUT40 = "HOA: v1\n"
      + "name: \"GFa\"\n"
      + "States: 11\n"
      + "Start: 0\n"
      + "acc-name: Buchi\n"
      + "Acceptance: 1 Inf(0)\n"
      + "AP: 2 \"0\" \"1\"\n"
      + "--BODY--\n"
      + "State: 0 {0}\n"
      + "[0] 1\n"
      + "[0] 2\n"
      + "[0] 5\n"
      + "[1] 0\n"
      + "State: 1 {0}\n"
      + "[0] 9\n"
      + "[1] 10\n"
      + "State: 2 {0}\n"
      + "[0] 1\n"
      + "[1] 1\n"
      + "State: 3 {0}\n"
      + "[0] 6\n"
      + "[1] 8\n"
      + "[1] 2\n"
      + "State: 4 {0}\n"
      + "[0] 1\n"
      + "[1] 10\n"
      + "State: 5 {0}\n"
      + "[1] 0\n"
      + "[1] 9\n"
      + "[1] 6\n"
      + "[0] 10\n"
      + "State: 6 {0}\n"
      + "[0] 7\n"
      + "[1] 3\n"
      + "State: 7 {0}\n"
      + "[0] 9\n"
      + "[1] 10\n"
      + "State: 8 {0}\n"
      + "[1] 9\n"
      + "[0] 10\n"
      + "State: 9 {0}\n"
      + "[0] 5\n"
      + "[1] 1\n"
      + "State: 10\n"
      + "\n"
      + "--END--";

  static {
    zero = new BitSet();
    zero.set(0);
    one = new BitSet();
    one.set(1);
  }

  @Before
  public void before() {

  }

  public Automaton<AutomatonReader.HoaState, OmegaAcceptance> parseAut(String input)
      throws ParseException {
    var supplier = DefaultEnvironment.annotated().factorySupplier();
    var automaton = AutomatonReader
        .readHoa(input, supplier::getValuationSetFactory, OmegaAcceptance.class);
    return automaton;
  }

  @Test
  public void test_cpre_usingFromSlidesAut() throws ParseException {
    var automaton = parseAut(FROMSLIDES);
    Set<HoaState> acceptingStates = UniversalityCheck.getAcceptingStates(automaton);
    assertEquals(acceptingStates.size(), 6);
    var nonAcceptingStates = new HashSet<>(automaton.states());
    nonAcceptingStates.removeAll(acceptingStates);
    assertEquals(nonAcceptingStates.size(), 2);
    Int2ObjectMap<HoaState> states = new Int2ObjectLinkedOpenHashMap<>(
        automaton.size());
    automaton.states().forEach(state -> {
      var stateId = Integer.parseInt(state.toString().split(" ")[0]);
      states.put(stateId, state);
    });
    assertEquals(states.get(0), automaton.onlyInitialState());
    assertTrue(nonAcceptingStates.contains(states.get(5)));
    assertTrue(nonAcceptingStates.contains(states.get(6)));
    var destinations = Set.of(states.get(5), states.get(6));
    // cpre(sigma = 0, s = {5, 6}) = {3}
    assertEquals(UniversalityCheck.cpre(automaton, zero, destinations),
        Set.of(states.get(3)));
    // cpre(sigma = 1, s = {5, 6}) = {3, 4}
    assertEquals(UniversalityCheck.cpre(automaton, one, destinations),
        Set.of(states.get(3), states.get(4)));
  }

  @Test
  public void test_cpre_aut40() throws ParseException {
    var automaton = parseAut(AUT40);
    Set<HoaState> acceptingStates = UniversalityCheck.getAcceptingStates(automaton);
    // state 4 is non reachable, so 9 reachable states out of 10
    assertEquals(acceptingStates.size(), 9);
    var nonAcceptingStates = new HashSet<>(automaton.states());
    nonAcceptingStates.removeAll(acceptingStates);
    assertEquals(nonAcceptingStates.size(), 1);
    Int2ObjectMap<HoaState> states = new Int2ObjectLinkedOpenHashMap<>(
        automaton.size());
    automaton.states().forEach(state -> {
      var stateId = Integer.parseInt(state.toString().split(" ")[0]);
      states.put(stateId, state);
    });
    assertEquals(states.get(0), automaton.onlyInitialState());
    assertTrue(nonAcceptingStates.contains(states.get(10)));
    // cpre(sigma = 0, s = {1, 7}) = {2, 6}
    var destinations = Set.of(states.get(1), states.get(7));
    assertEquals(UniversalityCheck.cpre(automaton, zero, destinations),
        Set.of(states.get(2), states.get(6)));
    // cpre(sigma = 1, s = {1, 7}) = {9, 2}
    assertEquals(UniversalityCheck.cpre(automaton, one, destinations),
        Set.of(states.get(9), states.get(2)));
  }
}