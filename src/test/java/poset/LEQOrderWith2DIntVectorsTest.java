package poset;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.List;
import org.junit.Before;
import org.junit.Test;
import poset.exceptions.Incomparable;
import poset.orders.LEQ;
import poset.orders.Order;

public class LEQOrderWith2DIntVectorsTest {
  private Order<List<Integer>> order;
  private List<Integer> v12, v34, v43;

  @Before
  public void beforeEachTest() {
    order = new LEQ();
    v34 = List.of(3, 4);
    v43 = List.of(4, 3);
    v12 = List.of(1, 2);
  }

  @Test
  public void LowerOrEqual_True() {
    assertTrue(order.test(v12, v34));
    assertTrue(order.test(v12, v43));
  }

  @Test
  public void LowerOrEqual_False() {
    assertFalse(order.test(v34, v12));
    assertFalse(order.test(v43, v12));
  }

  @Test
  public void Incomparable_WithSafeComparison() {
    assertFalse(order.test(v34, v43));
  }

  @Test(expected = Incomparable.class)
  public void Incomparable_WithIncomparableException() throws Incomparable {
    assertFalse(order.test(v34, v43));
  }

  @Test
  public void AreComparableTest() {
    assertFalse(order.areComparable(v34, v43));
    assertTrue(order.areComparable(v34, v12));
    assertTrue(order.areComparable(v43, v12));
  }

}
