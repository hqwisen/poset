package poset;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import poset.bounds.Bound;
import poset.exceptions.ComputationError;
import poset.orders.Order;


public class AntichainList<E> implements Antichain<E> {
  private final List<E> elements;
  private final Order<E> order;
  private final Bound<E> bound;


  public AntichainList(Order<E> order) {
    this(order, null);
  }

  public AntichainList(Order<E> order, Bound<E> bound) {
    this.elements = new LinkedList<>();
    this.order = order;
    this.bound = bound;
  }


//  public AntichainList(Order<E> order, Collection<E> c) {
//    this.elements = new LinkedList<>(c);
//    this.order = order;
//  }

  @Override
  public int size() {
    return elements.size();
  }

  @Override
  public boolean isEmpty() {
    return elements.isEmpty();
  }

  @Override
  public boolean contains(Object element) {
    return elements.contains(element);
  }

  @Override
  public Iterator<E> iterator() {
    return elements.iterator();
  }

  @Override
  public Object[] toArray() {
    return elements.toArray();
  }

  @Override
  public <T> T[] toArray(T[] ts) {
    throw new UnsupportedOperationException("Not implemented");
  }

  /**
   * Add an element to the antichain.
   * If the new element is smaller than any antichain elements, it is not added and the
   * addition stops.
   * All elements in the antichain smaller than the new one are removed.
   * The new element is added if it is bigger than one of the element of the antichain,
   * or if it is incomparable with all of them.
   *
   * @param element
   *     new element to add in the closed set.
   */
  private boolean symbolicAdd(E element) {
    Iterator<E> i = iterator();
    while (i.hasNext()) {
      E a = i.next();
      switch (order.icompare(a, element)) {
        case Order.SMALLER:
          i.remove();
          break;
        case Order.LARGER:
          return false;
        default:
          break;
      }
    }
    elements.add(element);
    return true;
  }

  @Override
  public boolean add(E element) {
    if (element == null) {
      return false;
    }
    return symbolicAdd(element);
  }

  @Override
  public boolean remove(Object o) {
    return false;
  }

  @Override
  public Antichain<E> union(Antichain<E> other) {
    // FIXME what happened if orders are different ?
    Antichain<E> result = new AntichainList<>(this.order);
    result.addAll(this.elements());
    result.addAll(other.elements());
    return result;
  }

  /**
   * Compute the bound between two antichains.
   * The computation consists into building a new antichain representation of the bound values
   * of all elements of both antichains.
   */
  public Antichain<E> computeBound(Antichain<E> other) {
    Bound<E> boundOp = this.bound;
    if (boundOp == null) {
      throw new ComputationError("Bound is not specified. " +
          "Cannot compute the bound of the two closed sets.");
    }
    Antichain<E> result = new AntichainList<>(this.order, this.bound);
    for (E a : this.elements()) {
      for (E b : other.elements()) {
        result.add(boundOp.compute(a, b, this.order));
      }
    }
    return result;
  }

  /**
   * Compute the intersection of two closed sets.
   * The intersection consists of the closure of the bound computation.
   */
  public Antichain<E> intersection(Antichain<E> other) {
    return computeBound(other);
  }

  @Override
  public boolean containsAll(Collection<?> collection) {
    return false;
  }

  @Override
  public boolean addAll(Collection<? extends E> elements) {
    if (elements == null) {
      return false;
    }
    boolean changed = false;
    for (E element : elements) {
      changed = add(element) || changed;
    }
    return changed;
  }

  @Override
  public boolean retainAll(Collection<?> collection) {
    throw new UnsupportedOperationException("Not implemented");
  }

  @Override
  public boolean removeAll(Collection<?> collection) {
    return elements.removeAll(collection);
  }

  public Collection<E> elements() {
    return this.elements;
  }

  @Override
  public Order<E> order() {
    return this.order;
  }

  @Override
  public void clear() {
    elements.clear();
  }

  @Override
  public String toString() {
    return elements.toString();
  }
}
