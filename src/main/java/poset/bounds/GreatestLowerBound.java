package poset.bounds;

import java.util.Arrays;
import java.util.List;
import poset.exceptions.ComputationError;
import poset.exceptions.Incomparable;
import poset.orders.Order;

public class GreatestLowerBound implements Bound<List<Integer>> {

  /**
   * Compute the greatest lower bound between two integer vectors, a `bound` b.
   * If vector are comparable: returns a if a `order` b, otherwise returns b.
   * If a and b are incomparable: returns a vector where components are the smallest
   * value of the component at the same index of both vectors a and b.
   *
   * @param order
   *     Partial order to use to compare the vectors.
   *
   * @return An integer vector that is the greatest lower bound between a and b.
   */
  @Override
  public List<Integer> compute(List<Integer> a, List<Integer> b,
      Order<List<Integer>> order) {
    try {
      if (order.compare(a, b)) {
        return a;
      } else {
        return b;
      }
    } catch (Incomparable e0) {
      // a and b are the same size. If different, compare will fail.
      Integer[] elements = new Integer[a.size()];
      List<Integer> aV1d, bV1d; // Vector of 1 dimension
      for (int i = 0; i < a.size(); i++) {
        // FIXME ugly to allocate memory at each loop
        aV1d = List.of(a.get(i));
        bV1d = List.of(b.get(i));
        try {
          elements[i] = order.compare(aV1d, bV1d) ? a.get(i) : b.get(i);
        } catch (Incomparable e1) {
          // should never happened with comparison of 1d vector.
          throw new ComputationError("Comparison vector of 1 dimension failed.");
        }
      }
      return Arrays.asList(elements);
    }
  }
}
