package poset.bounds;


import poset.orders.Order;

/**
 * Implements the lower bound of $P \subseteq S$ (for a poset $\langle S, \preceq \rangle$).
 */
@FunctionalInterface
public interface Bound<T> {
  /**
   * Computation of the bound operation $a \sqcap b$
   * @param a First element of the bound operation
   * @param b Second element of the bound operation
   * @param order order of the poset
   * @return the lower bound of $P$
   */
  T compute(T a, T b, Order<T> order);
}