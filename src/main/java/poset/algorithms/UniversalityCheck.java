package poset.algorithms;

import java.util.BitSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.logging.Logger;
import owl.automaton.Automaton;
import owl.automaton.AutomatonUtil;
import owl.automaton.acceptance.OmegaAcceptance;
import owl.factories.ValuationSetFactory;
import owl.run.modules.ImmutableTransformerParser;
import owl.run.modules.InputReaders;
import owl.run.modules.OutputWriters;
import owl.run.modules.OwlModuleParser.TransformerParser;
import owl.run.parser.PartialConfigurationParser;
import owl.run.parser.PartialModuleConfiguration;
import poset.Antichain;
import poset.AntichainList;
import poset.orders.Include;
import poset.orders.SubsetEq;

public final class UniversalityCheck
    implements
    Function<Automaton<Object, OmegaAcceptance>, UniversalityCheck.UniversalityCheckOutput> {

  private static final Logger logger = Logger.getLogger(UniversalityCheck.class.getName());

  public static Automaton<Object, OmegaAcceptance> A = null;
  public static final TransformerParser CLI = ImmutableTransformerParser.builder()
      .key("universality-check")
      .description("Check the universality of an automaton")
      .parser(settings -> {
        var function = new UniversalityCheck();
        return environment -> (input, context) ->
            function.apply(AutomatonUtil.cast(input, Object.class, OmegaAcceptance.class));
      }).build();
  Map<Object, Map<BitSet, Set<Object>>> successors;
  Map<Object, Set<Object>> predecessors;


  public UniversalityCheck() {
    this.successors = new HashMap<>();
    this.predecessors = new HashMap<>();
  }

  public void buildTransitions(Automaton<Object, ?> automaton) {
    // successors Map is the implementation of the function delta(state, letter) = [states]
    // predecessors Map is the implementation of  delta^{-1}
    var factory = automaton.factory();
    automaton.states().forEach(state -> {
      Map<BitSet, Set<Object>> stateSuccessors = new HashMap<>();
      for (int bit = 0; bit < factory.alphabetSize(); bit++) {
        var valuation = new BitSet();
        valuation.set(bit);
        stateSuccessors.put(valuation, automaton.successors(state, valuation));
        predecessors.put(state, automaton.predecessors(state));
      }
      successors.put(state, stateSuccessors);
    });
  }


  /**
   * z
   * 1. HashMap with keys = states and values = pred. or succs.
   * 2. Boolean vars for edges
   */
  public <S> Set<S> cpre(Automaton<S, ?> automaton,
      BitSet valuation, Set<S> destinations) {
    Set<S> predecessors = new HashSet<>();
    Set<S> result = new HashSet<>();
    destinations.forEach(destination -> {
      predecessors.addAll((Set<S>)this.predecessors.get(destination));
//      var ps = automaton.predecessors(destination);
//      predecessors.addAll(ps);
    });
    // TODO valuation should only represent one letter
    predecessors.forEach(predecessor -> {
      boolean containsAll = destinations.containsAll(successors.get(predecessor).get(valuation));
      if (containsAll) {
        result.add(predecessor);
      }
    });
//    logger.fine("Computing cpre_A(letter=" + valuation + ", q=" + destinations + ") = " + result);
    return result;
  }

  // TODO two methods to make this better:

  /**
   * Return an antichain of maximal set of states where there exists
   * a letter and a set of state from destinations such that for all states in
   * a maximal set, the resulting destinations going from those states reading
   * the letter is included in a subset of destinations.
   *
   * CPre (bad) implementation from the main article
   */
  public <S> Antichain<Set<S>> CPre(Automaton<S, ?> automaton, Set<Set<S>> destinations) {
    ValuationSetFactory factory = automaton.factory();
    Antichain<Set<S>> result = new AntichainList<>(Include::order);
    destinations.forEach(subset -> {
      var valuation = new BitSet();
      // TODO is there a method in ValuationSetFactory that does this, like forEach universe ?
      // TODO maybe use ValuationSet with all letters + factory.forEach(valuation) ?
      // for each letter
      for (int bit = 0; bit < factory.alphabetSize(); bit++) {
        valuation.clear();
        valuation.set(bit);
        result.add(cpre(automaton, valuation, subset));
      }
    });
//    logger.fine("Computing CPre_A(q=" + destinations + ") = " + result);
    return result;
  }

  // FIXME this description is probably wrong..

  private <S> Set<Set<S>> computeFrontier(Automaton<S, ?> automaton, Set<Set<S>> frontier,
      Set<Set<S>> F) {
    SubsetEq<S> order = new SubsetEq<>();
    // FIXME reuse same memory space allocated to frontier instead of reallocate ?
    Set<Set<S>> result = new HashSet<>();
    CPre(automaton, frontier).forEach(q -> {
      // FIXME this comparison might me wrong. Need to check Algorithm 1.
      var check = order.test(Set.of(q), F);
//      logger.fine(Set.of(q) + " <SubsetEq> " + F + " = " + check + " (if false -> add)");
      if (!check) {
        result.add(q);
      }
    });
    return result;
  }

  public <S, A extends OmegaAcceptance> UniversalityCheckOutput backward(
      Automaton<S, A> automaton) {
    Set<S> acceptingStates = getAcceptingStates(automaton);
//    logger.fine("Initial states: " + automaton.initialStates());
//    logger.fine("Accepting states: " + acceptingStates);
//    logger.fine("Automaton ValuationSet factory alphabet: " + automaton.factory().alphabet());
    var start = Set.of(automaton.initialStates());
    Set<S> nonAcceptingStates = new HashSet<>(automaton.states());
    nonAcceptingStates.removeAll(acceptingStates);
    Antichain<Set<S>> targets = new AntichainList<>(Include::order);
    targets.add(nonAcceptingStates);
    Set<Set<S>> frontier = new HashSet<>(targets);
//    logger.fine("Backward algorithms with the following initial data (Iteration #0):");
//    logger.fine("Start (Start): " + start);
//    logger.fine("targets (F): " + targets);
//    logger.fine("Frontier: " + frontier);
    SubsetEq<S> order = new SubsetEq<>();
    var iteration = 1;
    // FIXME what if start and frontier incomparable
//    long startTime = System.nanoTime();
    long startTime = System.currentTimeMillis();
    while (!frontier.isEmpty() && !order.test(start, frontier)) {
      frontier = computeFrontier(automaton, frontier, targets);
      targets.addAll(frontier);
//      logger.fine("Iteration #" + iteration);
//      logger.fine("Frontier: " + frontier);
//      logger.fine("targets (F): " + targets);
      iteration++;
    }
//    long endTime = System.nanoTime();
    long endTime = System.currentTimeMillis();
//    long duration = (endTime - startTime) / 1000000; // ms
    long duration = (endTime - startTime); // ms
    var result = !order.test(start, frontier);
    return new UniversalityCheckOutput(result, duration);

  }

  /**
   * Retrieve accepting states based on transition acceptance.
   * Because HOA consider state-based acceptance as syntactic sugar for transition acceptance,
   * this function consider a state to be accepting if all outgoing edges have an accepting set.
   */
  public static <S, A extends OmegaAcceptance> Set<S> getAcceptingStates(
      Automaton<S, A> automaton) {
    Set<S> acceptingStates = new HashSet<>();
    for (var state : automaton.states()) {
      var edges = automaton.edges(state);
//      boolean isAccepting = !edges.isEmpty();
      boolean isAccepting = false;
      for (var edge : edges) {
//        logger.fine("Checking edge: " + state + edge);
        if (edge.hasAcceptanceSets()) {
          isAccepting = true;
          break;
        }
      }
      if (isAccepting) {
        acceptingStates.add(state);
      }
    }
    return acceptingStates;
  }

  @Override
  public UniversalityCheckOutput apply(Automaton<Object, OmegaAcceptance> automaton) {
    if (!automaton.is(Automaton.Property.COMPLETE)) {
      throw new IllegalArgumentException("Cannot check universality using antichains"
          + " with incomplete automaton");
    }
    this.buildTransitions(automaton);
    return backward(automaton);
  }

  public static void main(String... args) {
    PartialConfigurationParser.run(args, PartialModuleConfiguration.builder("universality-check")
        .reader(InputReaders.HOA)
        .addTransformer(UniversalityCheck.CLI)
        .writer(OutputWriters.TO_STRING)
        .build());
  }


  static final class UniversalityCheckOutput {
    Boolean result;
    Long time;

    public UniversalityCheckOutput(Boolean result, Long time) {
      this.result = result;
      this.time = time;
    }

    @Override
    public String toString() {
      return result.toString() + ' ' + time;
    }
  }
}
