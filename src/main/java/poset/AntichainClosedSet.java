package poset;

import java.util.Collection;
import poset.orders.Order;

/**
 * Closed set implementation using the antichain canonical representation.
 */
public abstract class AntichainClosedSet<E> implements ClosedSet<E> {

  protected final Order<E> order;

  public AntichainClosedSet(Order<E> order) {
    this.order = order;
  }
  // FIXME Why antichainAdd ?
  protected abstract boolean antichainAdd(E element);

  /**
   * Return a collection of the incomparable elements of the antichain.
   *
   * @return collection of the incomparable elements
   */
  public abstract Antichain<E> antichain();

  public int antichainSize() {
    return antichain().size();
  }

  public boolean isEmpty() {
    return antichain().isEmpty();
  }

  @Override
  public Collection<E> elements() {
    return antichain();
  }
}