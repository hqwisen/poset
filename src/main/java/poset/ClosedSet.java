package poset;

import java.util.Collection;

/**
 * Interface for closed set representation.
 *
 * A closed set is a subset of $S$ and closed for a partial order $\preceq$ of a lower semilattice
 * $\langle S, \preceq \rangle$.
 * This interface provides the basic methods that must be supported for interacting with closed
 * sets.
 *
 * @param <E>
 *     type of element $s$ of the closed set, with $s \in S$.
 */
public interface ClosedSet<E> {

  /**
   * Add an element to the closed set.
   *
   * @param e
   *     element to add in the closed set.
   *
   * @return {@code true} if the closed set internal representation has changed
   */
  boolean add(E e);

  /**
   * Add all of the elements in the specified collection.
   *
   * @param c
   *     collection containing the elements to be added to this closed set
   *
   * @return {@code true} if the closed set internal representation has changed
   */
  boolean addAll(Collection<? extends E> c);

  /**
   * Each implementation should specify whether it uses the canonical representation
   * of a closed set.
   *
   * @return {@code true} if the elements are stored using a canonical representation
   */
  boolean isCanonical();

  /**
   * Return true if the element is in the closed set.
   *
   * @param e
   *     element whose presence in the closed set is to be tested
   *
   * @return {@code true} if closed set contains the element
   */
  boolean contains(E e);

  // TODO add javadoc
  ClosedSet<E> union(ClosedSet<E> other);

  /**
   * Compute the intersection of two closed sets.
   * The intersection consists of the closure of the bound computation.
   */
  ClosedSet<E> intersection(ClosedSet<E> other);

  boolean isEmpty();

  default int size() {
    return elements().size();
  }


  /**
   * Return the elements of the closed set.
   *
   * If the closed set uses a cononical representation, this method should only return
   * the element of such representation. If all the elements are required,
   * use {@link ClosedSet#closureElements()}  closureElements} method.
   *
   * @return collection of elements composing the closed set
   */
  Collection<E> elements();

  /**
   * Return all the elements contained in the closed set.
   *
   * Return the lower closure $\darrow \ceil{L}$ of the closed set,
   * i.e. all the elements of the closed set.
   *
   * @return all the elements of the closed set
   */
  Collection<E> closureElements();


  default int closureSize() {
    return closureElements().size();
  }
}