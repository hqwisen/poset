package poset.exceptions;

public class Incomparable extends Exception {

    public Incomparable() {
        super();
    }

    public Incomparable(String message) {
        super(message);
    }
}
