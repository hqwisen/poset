package poset.exceptions;

public class ComputationError extends RuntimeException {

    public ComputationError(String message) {
        super(message);
    }
}
