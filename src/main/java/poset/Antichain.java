package poset;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;
import poset.orders.Order;

/**
 * Antichain interface to implement a set of incomparable elements.
 *
 * Antichain $\alpha$ of a poset $\langle S, \preceq \rangle$ where $\alpha \subseteq S$.
 * The implementations of this class should use an order
 * (implementing the $\preceq$ partial order of the poset)
 * and store a collection of incomparable elements.
 *
 * @param <E>
 *     Type of the elements from the set $S$.
 */
@SuppressWarnings("override")
public interface Antichain<E> extends Set<E> {
  /**
   * @return the number of incomparable elements.
   */
  int size();

  /**
   * @return true if the antichain contains no elements, false otherwise.
   */
  boolean isEmpty();

  /**
   * Return true if the element is on of the incomparable elements of the antichain.
   *
   * @param o
   *     element whose presence in the antichain is to be tested
   *
   * @return \texttt{true} if antichain contains the element
   */
  boolean contains(Object o);

  /**
   * Add an element to the antichain.
   *
   * If the element is smaller of any element of the antichain, no operation will be performed.
   * If the element is bigger than any of the element of the antichain, it is added to the
   * sequence of incomparable elements, and every elements that are smaller to the new one
   * will be removed.
   *
   * @param e
   *     element to add.
   *
   * @return true if the element was added to the incomparable elements, false otherwise.
   */
  boolean add(E e);

  /**
   * Remove an element from the incomparable elements.
   *
   * @param o
   *     element to remave
   *
   * @return true if the element was removed, false otherwise.
   */
  boolean remove(Object o);

  /**
   * Perform an union operation between two antichains.
   * The results is a new antichain that contains the incomparable elements of both antichains.
   *
   * @return New antichain representing the union of both antichains.
   */
  Antichain<E> union(Antichain<E> other);

  /**
   * Perform an intersection between two antichains.
   *
   * @return a new antichain, that is the intersection.
   */
  Antichain<E> intersection(Antichain<E> other);

  boolean containsAll(Collection<?> collection);

  boolean addAll(Collection<? extends E> collection);

  boolean retainAll(Collection<?> collection);

  boolean removeAll(Collection<?> collection);

  Collection<E> elements();

  /**
   * Order $\preceq$ of the poset.
   *
   * @return the order
   */
  Order<E> order();

  void clear();

  Iterator<E> iterator();

  Object[] toArray();

  <T> T[] toArray(T[] ts);
}
