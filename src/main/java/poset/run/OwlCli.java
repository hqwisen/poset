package poset.run;



import owl.run.modules.OwlModuleRegistry;
import poset.algorithms.UniversalityCheck;

public final class OwlCli {
  public static void main(String[] args) {
    OwlModuleRegistry.DEFAULT_REGISTRY.register(UniversalityCheck.CLI);
    owl.run.DefaultCli.main(args);
  }
}
