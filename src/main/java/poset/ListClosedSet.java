//package poset;
//
//import java.util.Collection;
//import poset.bounds.Bound;
//import poset.orders.Order;
//
//
///**
// * Implementation of closed set using linked list to store the antichain.
// */
//public class ListClosedSet<E> implements ClosedSet<E> {
//
//  private final Order<E> order;
//  private final Antichain<E> antichain;
//  private final Bound<E> bound;
////  private Closure<T> closure;
//
//  public ListClosedSet(Order<E> order) {
//    this(order, null);
//  }
//
//
//  public ListClosedSet(Order<E> order, Bound<E> bound) {
//    this.order = order;
//    this.bound = bound;
//    this.antichain = new AntichainList<>(order);
//
//  }
////
////  public LinkedClosedSet(Collection<T> initials, Order<T> order) {
////    this(initials, order, null, null);
////  }
////
////  public LinkedClosedSet(Collection<T> initials, Order<T> order, Bound<T> bound,
////      Closure<T> closure) {
////    this.antichain = new LinkedList<>();
////    this.order = order;
////    this.bound = bound;
////    this.closure = closure;
////    this.addAll(initials);
////  }
//
//  @Override
//  public boolean add(E element) {
//    return antichain.add(element);
//  }
//
//  @Override
//  public boolean addAll(Collection<? extends E> elements) {
//    return antichain.addAll(elements);
//  }
//
//  @Override
//  public boolean isCanonical() {
//    return true;
//  }
//
//  @Override
//  public boolean contains(E element) {
//    if (element != null) {
//      for (E a : this.antichain) {
//        if (element.equals(a) || order.test(element, a)) {
//          return true;
//        }
//      }
//    }
//    return false;
//  }
//
//  @Override
//  public ClosedSet<E> union(ClosedSet<E> other) {
//    var result = new ListClosedSet<>(this.order);
//    this.elements().union(other.elements());
//    return result;
//  }
//
//  @Override
//  public ClosedSet<E> intersection(ClosedSet<E> other) {
//    return null;
//  }
//
//  @Override
//  public Collection<E> closureElements() {
//    throw new UnsupportedOperationException("Not implemented");
//  }
//
//
////  /**
////   * Returns all the elements of the closed set. The collection returned consists of all the
////   * elements that were added to the closed set.
////   */
////  public List<TE closureElements() {
////    if (closure == null) {
////      throw new IllegalArgumentException("Cannot compute closure without closure operation.");
////    }
////    for (T element : antichain) {
////
////    }
////    return null;
////  }
//
////  /**
////   * Total size of all the elements.
////   */
////  public int closureSize() {
////    return closureElements().size();
////  }
//
//
//  public boolean isEmpty() {
//    return antichain.isEmpty();
//  }
//
//  @Override
//  public Collection<E> elements() {
//    return antichain;
//  }
//
//  @Override
//  public String toString() {
//    return antichain.toString();
//  }
//
//  public boolean equals(Object o) {
//    if (o == this) {
//      return true;
//    }
//    if (!(o instanceof ClosedSet || o instanceof Collection)) {
//      return false;
//    }
//    var collection = o instanceof ClosedSet ? ((ClosedSet) o).elements() : o;
//    return antichain.equals(collection);
//  }
//}