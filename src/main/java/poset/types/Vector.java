package poset.types;


import java.util.Arrays;

/**
 * @deprecated This class is useless. Use built-int Vector/ArrayList class.
 */
@Deprecated
public class Vector<T> {

  private final T[] elements;

  public Vector(T... elements) {
    this.elements = elements.clone();
  }

  public int size() {
    return this.elements.length;
  }

  public T get(int index) {
    return this.elements[index];
  }

  public static boolean sameSize(Vector a, Vector b) {
    return a.size() == b.size();
  }

  @Override
  public boolean equals(Object o) {
    if (o == this) {
      return true;
    }
    if (!(o instanceof Vector)) {
      return false;
    }
    Vector other = (Vector) o;
    if (!Vector.sameSize(this, other)) {
      return false;
    }
    for (int i = 0; i < size(); i++) {
      if (!this.get(i).equals(other.get(i))) {
        return false;
      }
    }
    return true;
  }

  @Override
  public String toString() {
    return Arrays.toString(elements);
  }
}
