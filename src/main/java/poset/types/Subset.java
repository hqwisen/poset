package poset.types;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * @deprecated This class is useless. Use built-int Set class.
 */
@Deprecated
public class Subset<T> implements Iterable<T> {

    private Set<T> set;

    public Subset(T... elements) {
        this.set = new HashSet<>();
        this.set.addAll(Arrays.asList(elements));
    }

    public boolean includes(Subset<T> other) {
        for (T element : other.set) {
            if (!this.set.contains(element)) {
                return false;
            }
        }
        return true;
    }

    public boolean includesIn(Subset<T> other){
        return other.includes(this);
    }

    public boolean contains(T value){
        return this.set.contains(value);
    }

    public void add(T value){
        this.set.add(value);
    }

    @Override
    public Iterator<T> iterator() {
        return set.iterator();
    }

}
