package poset.orders;

import java.util.List;

/**
 * "Greater than or equal to" partial order for integer vectors.
 */
public class GEQ extends VectorOrder<Integer> {

  public static final GEQ ORDER = new GEQ();

  @Override
  protected boolean compareVector(List<Integer> a, List<Integer> b) {
    boolean smallerThan = true;
    for (int i = 0; i < a.size(); i++) {
      // FIXME if smaller than is false, can stop directly
      smallerThan = smallerThan && (a.get(i) >= b.get(i));
    }
    return smallerThan;
  }
}
