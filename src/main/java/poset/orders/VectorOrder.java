package poset.orders;

import java.util.List;

public abstract class VectorOrder<T> implements Order<List<T>> {
  /**
   * Compare two vectors. The vector must be the same dimension for the comparison the be done.
   *
   * @return true if a `order` b, false if b `order` a.
   */
  @Override
  public boolean test(List<T> a, List<T> b) {
    if (a.size() != b.size()) {
      throw new IllegalArgumentException("Cannot compare vectors with different dimension.");
    }
    return compareVector(a, b);
  }

  protected abstract boolean compareVector(List<T> a, List<T> b);
}
