package poset.orders;

import java.util.function.BiPredicate;
import poset.exceptions.Incomparable;

/**
 * Order interface.
 *
 * Boolean function that takes two parameters of the same type and return
 * \texttt{true} or \texttt{false}.
 */
@FunctionalInterface
public interface Order<T> extends BiPredicate<T, T> {

  short SMALLER = (short) 1;
  short LARGER = (short) 0;
  short INCOMPARABLE = (short) -1;

  /**
   * Compare two elements together.
   * A comparison is the result of the computation of  $a R b$.
   *
   * @param a
   *     First element to compare
   * @param b
   *     Second element to compare
   *
   * @return true if a `R` b, false  if b `R` a. If both are false, throws Incomparable.
   *
   * @throws Incomparable
   *     if both elements are incomparable.
   */
  default boolean compare(T a, T b) throws Incomparable {
    if (test(a, b)) {
      return true;
    }
    if (test(b, a)) {
      return false;
    }
    throw new Incomparable();
  }

  default short icompare(T a, T b) {
    if (test(a, b)) {
      return SMALLER;
    } else if (test(b, a)) {
      return LARGER;
    } else {
      return INCOMPARABLE;
    }
  }

  default boolean areComparable(T a, T b) {
    try {
      compare(a, b);
      return true;
    } catch (Incomparable e) {
      return false;
    }
  }

  /**
   * Evaluates the order. If the elements are incomparable no exception will be thrown,
   * and \texttt{false} will be returned.
   *
   * @param t1
   *     the first input argument
   * @param t2
   *     the second input argument
   *
   * @return {@code true} if $a R b$, false if $b R a$ or if both are false.
   */
  boolean test(T t1, T t2);

  /**
   * Negate (reverse) an order.
   *
   * @return The reversed order.
   */
  @Override
  default Order<T> negate() {
    return (T t1, T t2) -> !test(t1, t2);
  }
}

