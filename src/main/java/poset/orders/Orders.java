package poset.orders;

import java.util.Set;

public class Orders {

  public static <T> Order<Set<T>> include() {
    return (a, b) -> b.containsAll(a);
  }
}
