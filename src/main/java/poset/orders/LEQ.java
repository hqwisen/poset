package poset.orders;

import java.util.List;

/**
 * "Lower than or equal to" partial order for integer vectors.
 */
public class LEQ extends VectorOrder<Integer> {

  @Override
  protected boolean compareVector(List<Integer> a, List<Integer> b) {
    boolean smallerThan = true;
    for (int i = 0; i < a.size(); i++) {
      // FIXME if smaller than is false, can stop directly
      smallerThan = smallerThan && (a.get(i) <= b.get(i));
    }
    return smallerThan;
  }
}
