package poset.orders;

import java.util.Set;

/**
 * Inclusion partial order.
 *
 * @param <T>
 *     Type of element of the set.
 */
public class Include<T> implements Order<Set<T>> {

  @Override
  public boolean test(Set<T> a, Set<T> b) {
    return order(a, b);
  }

  /**
   * $a \subseteq b$
   *
   * @param a
   *     First element to compare
   * @param b
   *     Second element to compare
   *
   * @return True if $a \subseteq b$, false otherwise.
   */
  public static <T> boolean order(Set<T> a, Set<T> b) {
    return b.containsAll(a);
  }

}
