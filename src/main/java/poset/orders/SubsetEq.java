package poset.orders;

import java.util.Set;

public class SubsetEq<T> implements Order<Set<Set<T>>> {

  // TODO write test for this bound

  /**
   * Implementation of the the subset order.
   * <p>
   * $ q \sqsubseteq q'$
   *
   * @param a
   *     First element to compare
   * @param b
   *     Second element to compare
   */
  public boolean test(Set<Set<T>> a, Set<Set<T>> b) {
    return order(a, b);
  }

  public static <T> boolean order(Set<Set<T>> a, Set<Set<T>> b) {
    boolean containedInB;
    for (Set<T> subsetA : a) {
      containedInB = false;
      for (Set<T> subsetB : b) {
        if (subsetB.containsAll(subsetA)) {
          containedInB = true;
          break;
        }
      }
      if (!containedInB) {
        // If no subset of b contains the subset of a
        return false;
      }
    }
    return true;
  }

}
