package poset;

import java.util.Set;
import poset.orders.Order;
import poset.orders.Orders;

public class Playground {
  public static void main(String... args) {
    Set<Integer> s1, s2;
    s1 = Set.of(1, 2);
    s2 = Set.of(1);
    Order<Set<Integer>> include = Orders.include();
    Antichain<Set<Integer>> antichain = new AntichainList<>(include);
    antichain.add(Set.of(1, 2));
    antichain.add(Set.of(1));
    System.out.println(antichain);

  }

  static void antichainInterface() {
    Antichain<Set<Integer>> a = new AntichainList<>((x, y) -> y.containsAll(x));
    a.add(Set.of(1, 2)); a.add(Set.of(2, 1));
    // a ==> [[1, 2]]
    a.add(Set.of(3, 1, 2));
    // a ==> [[3, 1, 2]]

    a.union(a);
  }

}
